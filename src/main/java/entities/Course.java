package entities;

public class Course {
    private long id;

    public Course(long id, String name, String title) {
        this.id = id;
        this.name = name;
        this.title = title;
    }

    private String name;
    private String title;

    private Course() {
        super();
    }

}


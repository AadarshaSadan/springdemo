package controller;

import entities.Course;
import courcesservice.Courcesservices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Mycontroller {

    @Autowired
    private Courcesservices courcesservices_;

    @GetMapping("/home")
    public String home() {
        return "welcome to course";
    }


    @GetMapping("/cources")
    public List<Course> getCources() {
        return courcesservices_.getCources();
    }
}
